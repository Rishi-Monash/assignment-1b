"use strict";
console.log("MADE BY: RISHI BIDANI");
console.log("ASSIGNMENT 1B");

// Keys for localStorage
const STUDENT_INDEX_KEY = "studentIndex";
const STUDENT_QUEUE_KEY = "queueIndex";
const APP_DATA_KEY = "consultationAppData";

// ========================== TASK 1 ==================================== //
/**
 * This function will take a queue and return the shortest subqueue in it
 * The subqueue is just the shortest array in the main array
 * @param queue = n array of arrays
 * @return {{len: *, index: number}} = it will return an object that contains the length and its position
 */
function shortestQueue(queue) {
    // console.log(queue[0]);
    let currShortest = {
        index: 0,
        len: queue[0].length,
    };
    queue.forEach((elem, index) => {
        if (elem.length < currShortest.len) {
            currShortest.index = index;
            currShortest.len = elem.length;
        }
    });
    // console.log(currShortest);
    return currShortest;
}

class Student {
    constructor(fullname, studentId, problem) {
        this._fullname = fullname;
        this._studentId = studentId;
        this._problem = problem;
        this.regex = /^[1-3]{1}[0-9]{7}$/;
    }

    get fullname() {
        return this._fullname;
    }
    set fullname(fullname) {
        if (typeof fullname === "string") {
            this._fullname = fullname;
        }
    }

    get studentId() {
        return this._studentId;
    }
    set studentId(studentId) {
        if (studentId.match(this.regex)) {
            this._studentId = studentId;
        }
    }

    get problem() {
        return this._problem;
    }
    set problem(problem) {
        if (typeof problem === "string") {
            this._problem = problem;
        }
    }
    fromData(studentData) {
        if (typeof studentData === "object") {
            this.fullname = studentData._fullname;
            this.studentId = studentData._studentId;
            this.problem = studentData._problem;
        }
    }
}

class Session {
    constructor() {
        this._startTime = new Date();
        this._queue = [];
    }
    get startTime() {
        return this._startTime;
    }
    get queue() {
        return this._queue;
    }
    addSubQueue() {
        this._queue.push([]);
    }
    addStudent(fullname, id, problem) {
        console.log(this._queue);
        const shortestqueue = shortestQueue(this.queue);
        this._queue[shortestqueue.index].push(new Student(fullname, id, problem));
    }
    removeStudent(stdIndex, queueIndex) {
        this._queue[queueIndex].splice(stdIndex, 1);
    }
    getStudent(stdIndex, queueIndex) {
        return this._queue[queueIndex][stdIndex];
    }
    fromData(sessData) {
        sessData = sessData._queue;
        sessData.forEach((studentArr) => {
            studentArr.forEach((student, index) => {
                const classObj = new Student().fromData(student);
                if (typeof classObj === "object") {
                    sessData[index].push(classObj);
                } else {
                    return;
                }
            });
        });
        console.log(sessData);
        this._queue = sessData;
    }
}

// ========================== TASK 2 ==================================== //
// This function will check if the data for the given key exists
function checkLocalStorageDataExist(key) {
    if (localStorage.getItem(key)) {
        return true;
    }
    return false;
}

// ========================== TASK 3 ==================================== //
/**
 *  THis function is being used for updating the data in local storage
 * @param key = key for the data
 * @param {any} data = data
 */
function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}

// ========================== TASK 4 ==================================== //
/**
 * THis function will be used to find data in the local storage using a
 * particular key
 * @param key = key for which u want to find the data
 * @return {any} = data
 */
function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;
    try {
        data = JSON.parse(jsonData);
    } catch (e) {
        console.error(e);
    } finally {
        return data;
    }
}

// ========================== TASK 5 ==================================== //
let consultSession = new Session();
if (checkLocalStorageDataExist(APP_DATA_KEY)) {
    // if LS data does exist
    let data = getLocalStorageData(APP_DATA_KEY);
    consultSession.fromData(data);
} else {
    // if LS data doesn’t exist
    consultSession.addSubQueue();
    consultSession.addSubQueue();
    updateLocalStorageData(APP_DATA_KEY, consultSession);
}
