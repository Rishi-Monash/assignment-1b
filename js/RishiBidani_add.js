"use strict";

// ========================== TASK 6 ==================================== //
/**
 * This function will be used for adding Students to the queue
 * and it will also update the local storage data witht the
 * updated queue data
 */
function addStudent() {
    const stdName = document.querySelector("#fullName").value;
    const stdId = document.querySelector("#studentId").value;
    const stdDesc = document.querySelector("#problem").value;

    const stdNameErr = document.querySelector("#fullName_msg");
    const stdIdErr = document.querySelector("#studentId_msg");
    const stdDescErr = document.querySelector("#problem_msg");

    const regex = /^[1-3]{1}[0-9]{7}$/;

    if (stdName.length === 0) {
        stdNameErr.innerText = "Please Enter Full Name";
        return;
    } else if (stdId.length === 0) {
        stdIdErr.innerText = "Please Enter Student Id";
        return;
    } else if (stdDesc.length === 0) {
        stdDescErr.innerText = "Please Enter your problem";
        return;
    }

    // Checking if student ID entered mathces the regular format
    if (!stdId.match(regex)) {
        stdIdErr.innerText = "Please Enter Correct Student Id";
        return;
    }
    consultSession.addStudent(stdName, stdId, stdDesc);
    updateLocalStorageData(APP_DATA_KEY, consultSession);
    alert(`Student ${stdName} Added to the queue`);
    window.location = "./RishiBidani_index.html";
}
