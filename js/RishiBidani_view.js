"use strict";
// ========================== TASK 12 ==================================== //
const studentIndex = getLocalStorageData(STUDENT_INDEX_KEY);
const queueIndex = getLocalStorageData(STUDENT_QUEUE_KEY);

// HTML Element Selectors
const nameHolder = document.querySelector("#StudentName");
const stdId = document.querySelector("#StudentId");
const problemHolder = document.querySelector("#StudentProblem");

// console.log(consultSession.getStudent(studentIndex, queueIndex));

const currStudent = consultSession.getStudent(studentIndex, queueIndex);

nameHolder.innerText = currStudent._fullname;
stdId.innerHTML = "<strong>ID: </strong>" + currStudent._studentId;
problemHolder.innerHTML = "<strong>Problem</strong><br>" + currStudent._problem;

/**
 * This function is being used for marking the selected
 * student as "complete" and removing them from the queue
 * This function is for the view page
 */
function markDone() {
    if (confirm("Do you wish to delete this student from the queue?")) {
        consultSession.removeStudent(studentIndex, queueIndex);
        updateLocalStorageData(APP_DATA_KEY, consultSession);
        window.location = "./RishiBidani_index.html";
    }
}
