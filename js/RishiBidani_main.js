"use strict";
const timeNow = document.querySelector("#currentTime");
const queueContent = document.querySelector("#queueContent");

// ========================== TASK 7 ==================================== //
timeNow.innerText = new Date().toLocaleTimeString();
/**
 * Update the time every second, using a recursive function
 * @returns {string} = updates the time in #timeNow
 **/
function updateDayTime() {
    setTimeout(() => {
        timeNow.innerText = updateDayTime();
    }, 1000);
    return new Date().toLocaleTimeString();
}

updateDayTime();

// ========================== TASK 8 ==================================== //
function view(index, queueIndex) {
    updateLocalStorageData(STUDENT_INDEX_KEY, index);
    updateLocalStorageData(STUDENT_QUEUE_KEY, queueIndex);
    window.location = "./RishiBidani_view.html";
}

// ========================== TASK 9 ==================================== //
/**
 * This function will take the student index number and the queueIndex number
 * and will mark the student as "complete" and remove them from the queue
 * @param index = index of student in the queue
 * @param queueIndex = index of "this" subqueue in the main array queue
 */
function markDone(index, queueIndex) {
    if (confirm("Do you wish to delete this student from the queue?")) {
        consultSession.removeStudent(index, queueIndex);
        updateLocalStorageData(APP_DATA_KEY, consultSession);
        location.reload();
    }
}

// ========================== TASK 10 ==================================== //
/**
 * This funciton is being used for dispalying the queue status
 * @param queueData = queue data
 */
function queueStatus(queueData) {
    queueContent.innerHTML = "";
    for (let i = 0; i < queueData.length; i++) {
        // Create the heading for each queue
        const queueUL = document.createElement("ul");
        queueUL.classList.add("mdl-list");

        const headingH4 = document.createElement("h4");
        const headingTitle = document.createTextNode(`Queue ${i + 1}`);
        headingH4.appendChild(headingTitle);
        // Add Queue Container into queueContent
        queueUL.appendChild(headingH4);

        // Create the list of students in each queue
        queueData[i].forEach((studentData, index) => {
            const li = `
            <li class="mdl-list__item mdl-list__item--three-line">
                <span class="mdl-list__item-primary-content">
                    <i class="material-icons mdl-list__item-avatar">person</i>
                    <span>${studentData._fullname}</span>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a id="info-${i}-${index}" class="mdl-list__item-secondary-action" onclick="view(${index},${i})"><i class="material-icons">info</i></a>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a id="done-${i}-${index}" class="mdl-list__item-secondary-action" onclick="markDone(${index},${i})"><i class="material-icons">done</i></a>
                </span>

                <div class="mdl-tooltip" data-mdl-for="info-${i}-${index}">
                    Info
                </div>
                <div class="mdl-tooltip" data-mdl-for="done-${i}-${index}">
                    Done
                </div>

            </li>
            `;
            queueUL.innerHTML += li;
        });

        queueContent.appendChild(queueUL);
    }
}

queueStatus(consultSession.queue);
